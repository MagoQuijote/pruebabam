"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const carsController_1 = __importDefault(require("../controllers/carsController"));
class CarsRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/', carsController_1.default.list);
        this.router.get('/:id', carsController_1.default.getOne);
        this.router.post('/', carsController_1.default.create);
        this.router.put('/:id', carsController_1.default.update);
        this.router.delete('/:id', carsController_1.default.delete);
    }
}
const carsRoute = new CarsRoutes();
exports.default = carsRoute.router;
