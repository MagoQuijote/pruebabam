"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const carSchema = new mongoose_1.Schema({
    marca: {
        type: String,
        required: true,
        trim: true
    },
    modelo: {
        type: String,
        required: true,
        trim: true
    },
    color: {
        type: String,
        required: true,
        trim: true
    },
    matricula: {
        type: String,
        required: true,
        trim: true
    },
    precio: {
        type: Float64Array,
        required: true,
        trim: true
    }
});
exports.default = mongoose_1.model('car', carSchema);
