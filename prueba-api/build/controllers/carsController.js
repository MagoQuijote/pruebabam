"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cars_1 = __importDefault(require("../routes/cars"));
class CarsController {
    list(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const car = yield cars_1.default.find();
                return res.json(car);
            }
            catch (error) {
                res.json(error);
            }
        });
    }
    getOne(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const car = yield cars_1.default.findById(req.params.id);
            if (!car)
                return res.status(404).json({ text: 'El carro no existe' });
            ;
            return res.json(car);
        });
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const car = new cars_1.default(req.body);
            const savedCar = yield car.save();
            res.json(savedCar);
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const car = yield cars_1.default.findByIdAndDelete(req.params.id);
            if (!car)
                return res.status(404).json({ text: 'El carro no existe' });
            ;
            return res.json({ messaje: 'Car deleted' });
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const car = yield cars_1.default.findByIdAndUpdate(req.params.id, req.body, {
                new: true,
            });
            if (!car)
                return res.status(404).json({ text: 'El carro no existe' });
            ;
            return res.json({ messaje: 'Car Updated' });
        });
    }
}
const carsController = new CarsController();
exports.default = carsController;
