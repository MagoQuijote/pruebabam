import {Request, RequestHandler, Response} from 'express';
import Car from '../routes/cars';

class CarsController {
    
    public async list (req: Request,res: Response): Promise<void>{
        const car = await Car.find();
        res.json(car); 
    }

    public async getOne (req: Request,res: Response): Promise<any>{
        const car = await Car.findById(req.params.id);
        if (!car) return res.status(404).json({text: 'El carro no existe'});;

        return res.json(car);
        
    }
    
    public async create (req: Request,res: Response): Promise<void> {
        const car = new Car (req.body);
        const savedCar = await car.save();
        res.json(savedCar);
    }

    public async delete (req: Request,res: Response): Promise<any> {
        const car = await Car.findByIdAndDelete(req.params.id);
        if (!car) return res.status(404).json({text: 'El carro no existe'});;

        return res.json({messaje: 'Car deleted'});
    }

    public async update (req: Request,res: Response): Promise<Response> {
        const car = await Car.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
        });
        if (!car) return res.status(404).json({text: 'El carro no existe'});;

        return res.json({messaje: 'Car Updated'});
    }
}

const carsController = new CarsController();
export default carsController; 