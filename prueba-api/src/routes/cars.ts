import { Schema, model } from 'mongoose';

const carSchema = new Schema(
    {
        marca: {
            type: String,
            required: true,
            trim: true
        },
        modelo: {
            type: String,
            required: true,
            trim: true
        },
        color: {
            type: String,
            required: true,
            trim: true
        }, 
        matricula: {
            type: String,
            required: true,
            trim: true
        },
        precio: {
            type: Number,
            required: true,
            trim: true
        }
    },
    {
        versionKey: false,
        timestamps: true
    }
);

export default model ('Car', carSchema);
