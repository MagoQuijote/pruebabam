import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AngularMaterialModule } from './angular-material.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { NavbarComponent } from './pages/dashboard/navbar/navbar.component';
import { HomeComponent } from './pages/dashboard/home/home.component';
import { CarsComponent } from './pages/dashboard/cars/cars.component';
import { LoadingPageDialogComponent } from './components/shared/loading-page-dialog/loading-page-dialog.component';

//Services
import { CarService } from './core/services/car.service';
import { CarsCreateDialogComponent } from './pages/dashboard/cars/dialogs/cars-create-dialog/cars-create-dialog.component';
import { ConfirmDialogComponent } from './components/shared/confirm-dialog/confirm-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    NavbarComponent,
    HomeComponent,
    CarsComponent,
    LoadingPageDialogComponent,
    CarsCreateDialogComponent,
    ConfirmDialogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule
  ],
  providers: [
    CarService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
