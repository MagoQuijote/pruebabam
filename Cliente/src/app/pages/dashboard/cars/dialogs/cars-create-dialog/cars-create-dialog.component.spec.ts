import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarsCreateDialogComponent } from './cars-create-dialog.component';

describe('CarsCreateDialogComponent', () => {
  let component: CarsCreateDialogComponent;
  let fixture: ComponentFixture<CarsCreateDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarsCreateDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarsCreateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
