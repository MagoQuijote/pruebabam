import { Component, OnInit, Inject } from '@angular/core';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CarService } from 'src/app/core/services/car.service';


@Component({
  selector: 'app-cars-create-dialog',
  templateUrl: './cars-create-dialog.component.html',
  styleUrls: ['./cars-create-dialog.component.css']
})
export class CarsCreateDialogComponent implements OnInit {

  public carForm!: FormGroup;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<CarsCreateDialogComponent>,
    private carService: CarService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit(): void {
    if(this.data.car?._id){
      this.carForm = new FormGroup({
        _id: new FormControl(this.data.car?._id),
        marca: new FormControl(this.data.car?.marca),
        color: new FormControl(this.data.car?.color),
        modelo: new FormControl(this.data.car?.modelo),
        matricula: new FormControl(this.data.car?.matricula),
        precio: new FormControl(this.data.car?.precio)
      });
    } else {
      this.carForm = new FormGroup({
        _id: new FormControl('0'),
        marca: new FormControl(''),
        color: new FormControl(''),
        modelo: new FormControl(''),
        matricula: new FormControl(''),
        precio: new FormControl('')
      });
    }
    console.log(this._id?.value);
  }

  get _id() { return this.carForm.get('_id'); }
  get marca() { return this.carForm.get('marca'); }
  get color() { return this.carForm.get('color'); }
  get modelo() { return this.carForm.get('modelo'); }
  get matricula() { return this.carForm.get('matricula'); }
  get precio() { return this.carForm.get('precio'); }

  save(): void {
    const dialoadRef = this.dialog.open(LoadingPageDialogComponent, {
      width: '400px',
      data: {
        description: `Se`,
        title: 'Confirmación de cambios',
      },
      disableClose: true
    });
    if (this._id?.value == 0) {
      const element = this.carForm.value;
      var car={
        marca:element.marca,
        modelo:element.modelo,
        matricula: element.matricula,
        precio: element.precio,
        color: element.color
      }
      this.carService.saveCar(car).subscribe(response => {
        console.log(response);
        console.log('Se ha creado el carro' + this.marca?.value);
        this.dialogRef.close(true);
        dialoadRef.close();
      },
        err => {
          console.error(err);
          dialoadRef.close();
        });
    } else {
      const element = this.carForm.value;
      const idCar = this.carForm.value._id;
      var car={
        marca:element.marca,
        modelo:element.modelo,
        matricula: element.matricula,
        precio: element.precio,
        color: element.color
      }
      this.carService.editCar(car,idCar).subscribe(response => {
        console.log('Se ha modificado el carro' + this.marca?.value);
        this.dialogRef.close(true);
        dialoadRef.close();
      },
        err => {
          console.error(err);
          dialoadRef.close();
        });
      }

  }

}
