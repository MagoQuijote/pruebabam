import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CarService } from 'src/app/core/services/car.service';
import { CarResponse } from 'src/app/core/models/CarResponse';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { CarsCreateDialogComponent } from './dialogs/cars-create-dialog/cars-create-dialog.component';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {

  queryValue: string = "";
  cars: MatTableDataSource<CarResponse> = new MatTableDataSource<CarResponse>();
  loading = false;

  featureForm = new FormGroup({
    marca: new FormControl('',
      Validators.required),
    color: new FormControl('',
      Validators.required),
    modelo: new FormControl('',
      Validators.required),
    matricula: new FormControl('',
    Validators.required),
    Precio: new FormControl('',
    Validators.required)
  });


  constructor(
    private carService: CarService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.loadCars();
  }

  loadCars(){
    
    this.loading = true;
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.carService.getCars().subscribe (response => {
      if (response != null) {
        var car: any = [];
        car = response;
        this.cars = new MatTableDataSource<CarResponse>(car);
        this.applyFilter();
      } else {
        console.log('Vacio');
      }
      dialogRefLoadingPage.close();
      this.loading = false;
    },
      err => {
        console.log(err);
        dialogRefLoadingPage.close();
        this.loading = false;
      });
  }

  applyFilter() {
    this.cars.filter = this.queryValue.trim().toLowerCase();
  }

  deleteCar(idCar: string) {
    this.carService.deleteCar(idCar).subscribe(response => {
      if (response != null) {
          console.log('Se ha eliminado el carro');
          this.loadCars();
      } else {
        console.log('No es posible la acción')
      }
    }, 
      err => {console.error(err);
    });
  }

  editCar(car?: CarResponse) {
    const dialogActionDetail = this.dialog.open(CarsCreateDialogComponent, {
      width: '60%',
      data: {
        car: car
      }
    });
    dialogActionDetail.afterClosed().subscribe(result => {
      if (result) {
        this.loadCars();
      }
    });
  }

}
