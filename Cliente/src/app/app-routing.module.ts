import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';  
import { CarsComponent } from './pages/dashboard/cars/cars.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { HomeComponent } from './pages/dashboard/home/home.component';

                                                                                                               

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
   { path: 'dashboard', component: DashboardComponent, children:[
    { path: '', component: HomeComponent },
    { path: 'cars', component: CarsComponent },
   ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
