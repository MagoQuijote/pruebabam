export interface CarResponse {
    _id?: string;
    marca?: string;
    modelo?: string;
    color?: string;
    matricula?: string;
    precio?: number;
    createdAt?: string;
    updatedAt?: string;
};
