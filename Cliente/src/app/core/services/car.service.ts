import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CarResponse } from '../models/CarResponse';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  constructor(private http: HttpClient) { }

  public getCars() {
    return this.http.get(`${environment.apiBackEnd}/cars`);
  }

  public saveCar(param: any): Observable<CarResponse> {
    return this.http.post<CarResponse>(`${environment.apiBackEnd}/cars`, param);
  }

  public editCar(params: any, idCar:string): Observable<CarResponse> {
    return this.http.put<CarResponse>(`${environment.apiBackEnd}/cars/${idCar}`, params);
  }

  public deleteCar(idCar: string): Observable<any> {
    return this.http.delete(`${environment.apiBackEnd}/cars/${idCar}`);
  }

}
